import gql from "graphql-tag";

fragments = {
  entry: gql`
    fragment Details on Entry {
      id
      model
      issue
      cust_name
      cust_number
      cust_price
      repair_cost
      is_repaired
      is_delivered
      createdAt
    }
  `
};

export const SIGNIN = gql`
  mutation LogIn($input: UserInput!) {
    signIn(input: $input)
  }
`;

export const ALL_ENTRIES = gql`
  query {
    allEntries {
      ...Details
    }
  }
  ${fragments.entry}
`;

export const ADD_ENTRY = gql`
  mutation addEntry($input: EntryInput) {
    createEntry(input: $input) {
      ...Details
    }
  }
  ${fragments.entry}
`;

export const UPDATE_ENTRY = gql`
  mutation change($id: ID!, $input: EntryInput) {
    updateEntry(id: $id, input: $input) {
      ...Details
    }
  }
  ${fragments.entry}
`;

export const DELETE_ENTRY = gql`
  mutation remove($id: ID!) {
    deleteEntry(id: $id)
  }
`;
