import React, { useState } from "react";
import { Text, View, StyleSheet } from "react-native";
import { Divider, Icon, ListItem, Overlay } from "react-native-elements";
import { Layout } from "../Components";
import { withMappedNavigationParams } from "react-navigation-props-mapper";
import TouchableScale from "react-native-touchable-scale";
import DeleteItem from "./DeleteItem";

function Detail(props) {
  const [visible, setvisible] = useState(false);
  const [modal, showModal] = useState(false);
  const {
    id,
    model,
    issue,
    cust_name,
    cust_number,
    cust_price,
    repair_cost,
    is_delivered,
    is_repaired
  } = props.data;

  let repair;
  if (repair_cost) {
    if (visible) {
      repair = repair_cost;
    } else {
      repair = "*".repeat(repair_cost.toString().length);
    }
  } else {
    repair = "Not available";
  }

  const detailData = [
    {
      icon: "mobile1",
      title: "Model Name",
      subtitle: model
    },
    {
      icon: "warning",
      title: "Issue",
      subtitle: issue
    },
    {
      icon: "user",
      title: "Customer Name",
      subtitle: cust_name
    },
    {
      icon: "phone",
      title: "Contact Number",
      subtitle: cust_number
    },
    {
      icon: "tago",
      title: "Customer Price",
      subtitle: cust_price
    }
  ];

  return (
    <Layout
      title={"Detail"}
      rightComponent={
        <>
          <EditButton
            onPress={() =>
              props.navigation.navigate("Update", { data: props.data })
            }
          />
          <DeleteButton onPress={() => showModal(!modal)} />
        </>
      }>
      <View style={styles.container}>
        {detailData.map((item, index) => (
          <DetailItem key={index} {...item} />
        ))}
        <ListItem
          containerStyle={styles.item}
          title="Repair Cost"
          leftIcon={{
            name: "bank",
            type: "antdesign",
            size: 30,
            color: "#000"
          }}
          subtitle={repair.toString()}
          switch={{
            onValueChange: value => setvisible(!visible),
            value: visible
          }}
          titleStyle={styles.title}
          subtitleStyle={styles.subtitle}
        />
        <ListItem
          title="Repaired"
          subtitle={is_repaired ? "Yes" : "No"}
          leftIcon={{
            name: "tool",
            type: "antdesign",
            size: 26,
            color: "#000"
          }}
          rightIcon={<StatusIcon status={is_repaired} />}
        />
        <ListItem
          title="Delivered"
          subtitle={is_delivered ? "Yes" : "No"}
          leftIcon={{
            name: "shoppingcart",
            type: "antdesign",
            size: 26,
            color: "#000"
          }}
          rightIcon={<StatusIcon status={is_delivered} />}
        />
      </View>
      <Overlay
        borderRadius={15}
        containerStyle={styles.prompt}
        overlayStyle={styles.in}
        height={140}
        isVisible={modal}
        onBackdropPress={() => showModal(!modal)}>
        <DeleteItem id={id} goBack={() => showModal(!modal)} />
      </Overlay>
    </Layout>
  );
}

//Detail itme for showing common details
const DetailItem = props => (
  <ListItem
    containerStyle={styles.item}
    leftIcon={{
      name: props.icon,
      type: "antdesign",
      size: 30,
      color: "#000"
    }}
    title={props.title}
    titleStyle={styles.title}
    subtitle={props.subtitle ? props.subtitle.toString() : "Not available"}
    subtitleStyle={styles.subtitle}
  />
);

// status icon for repair and delivery status
const StatusIcon = props => (
  <Icon
    name={props.status ? "checkcircle" : "closecircle"}
    type="antdesign"
    color={props.status ? "green" : "red"}
    size={24}
  />
);

const hitSlop = { top: 12, left: 12, bottom: 12, right: 12 };

//editbutton for rightComponent in header
const EditButton = props => (
  <TouchableScale hitSlop={hitSlop} style={styles.options} {...props}>
    <Icon name="edit" type="antdesign" size={24} color="#000" />
  </TouchableScale>
);

const DeleteButton = props => (
  <TouchableScale hitSlop={hitSlop} style={styles.options} {...props}>
    <Icon name="delete" type="antdesign" size={24} color="red" />
  </TouchableScale>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 8
  },
  item: {
    paddingVertical: 10
  },
  subtitle: {
    fontSize: 16,
    fontWeight: "bold",
    color: "#000"
  },
  title: {
    fontSize: 14,
    fontWeight: "200"
  },
  options: {
    flex: 1,
    justifyContent: "space-evenly"
  },
  prompt: {
    flex: 1
  },
  in: {
    padding: 16
  }
});

export default withMappedNavigationParams()(Detail);
