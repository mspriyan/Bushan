import React from "react";
import { Text, View, ActivityIndicator, StyleSheet } from "react-native";
import { withNavigation } from "react-navigation";
import { Mutation } from "react-apollo";
import TouchableScale from "react-native-touchable-scale";
import { DELETE_ENTRY, ALL_ENTRIES } from "../api";

function DeleteItem(props) {
  return (
    <Mutation
      mutation={DELETE_ENTRY}
      update={cache => {
        const data = cache.readQuery({ query: ALL_ENTRIES });
        const index = data.allEntries.findIndex(entry => entry.id === props.id);
        data.allEntries.splice(index, 1);
        cache.writeQuery({
          query: ALL_ENTRIES,
          data
        });
      }}
      onCompleted={() => props.navigation.navigate("Homepage")}
      variables={{ id: props.id }}>
      {(deleteEntry, { data, loading, error }) => {
        if (loading) return <ActivityIndicator size="small" />;
        return (
          <>
            <Text style={styles.overText}>
              Are you sure you want to delete this Entry?
            </Text>
            <View style={styles.section}>
              <TouchableScale style={styles.inner} onPress={deleteEntry}>
                <Text style={styles.confirm}>Yes</Text>
              </TouchableScale>
              <TouchableScale style={styles.inner} onPress={props.goBack}>
                <Text style={styles.cancel}>No</Text>
              </TouchableScale>
            </View>
          </>
        );
      }}
    </Mutation>
  );
}

const styles = StyleSheet.create({
  overText: {
    flex: 2,
    fontSize: 19,
    fontWeight: "300",
    textAlign: "center"
  },
  confirm: {
    fontSize: 19,
    fontWeight: "300",
    textAlign: "center",
    color: "red"
  },
  cancel: {
    fontSize: 19,
    fontWeight: "300",
    textAlign: "center"
  },
  section: {
    flex: 1,
    flexDirection: "row"
  },
  inner: {
    flex: 1,
    justifyContent: "center"
  }
});

export default withNavigation(DeleteItem);
