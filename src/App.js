import React, { Component } from "react";
import ApolloClient from "apollo-boost";
import { InMemoryCache } from "apollo-cache-inmemory";
import { ApolloProvider } from "react-apollo";
import PathFinder from "./PathFinder";
import AsyncStorage from "@react-native-community/async-storage";

const client = new ApolloClient({
  uri: "https://kalanjiyam-git-master.niral.now.sh/graphql",
  // uri: "http://192.168.43.32:4000/graphql",
  cache: new InMemoryCache(),
  request: async operation => {
    const token = await AsyncStorage.getItem("TOKEN");
    operation.setContext({
      headers: {
        authorization: token ? `Bearer ${token}` : ""
      }
    });
  }
});

export default class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <PathFinder />
      </ApolloProvider>
    );
  }
}
