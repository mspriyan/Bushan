import React from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";
import { Input, CheckBox, Button } from "react-native-elements";
import { PrimaryButton } from "../Components";

export default function FormContainer(props) {
  const {
    model,
    issue,
    cust_name,
    cust_number,
    cust_price,
    repair_cost,
    is_repaired,
    is_delivered
  } = props.data;

  return (
    <ScrollView>
      <View style={styles.constainer}>
        <CustomInput
          placeholder={"Model Name"}
          onChangeText={text => props.handleChange("model", text)}
          value={model}
        />
        <CustomInput
          placeholder={"Issue"}
          onChangeText={text => props.handleChange("issue", text)}
          value={issue}
        />
        <CustomInput
          placeholder={"Customer Name"}
          onChangeText={text => props.handleChange("cust_name", text)}
          value={cust_name}
        />
        <CustomInput
          placeholder={"Customer Number"}
          keyboardType="numeric"
          onChangeText={text => props.handleChange("cust_number", text)}
          value={cust_number}
        />
        <View style={styles.miniContainer}>
          <Input
            keyboardType="numeric"
            containerStyle={{ flex: 1 }}
            inputContainerStyle={styles.input}
            placeholder="Price"
            onChangeText={text =>
              props.handleChange("cust_price", text ? parseInt(text) : "")
            }
            value={cust_price ? cust_price.toString() : ""}
          />
          <Input
            keyboardType="numeric"
            inputContainerStyle={styles.input}
            containerStyle={{ flex: 1 }}
            placeholder="Repair Cost"
            secureTextEntry={true}
            onChangeText={text =>
              props.handleChange("repair_cost", text ? parseInt(text) : "")
            }
            value={repair_cost ? repair_cost.toString() : ""}
          />
        </View>
        <View style={styles.miniContainer}>
          <CustomCheck
            title="Repaired"
            checked={is_repaired}
            onPress={() => props.handleCheck("is_repaired")}
          />
          <CustomCheck
            title="Delivered"
            checked={is_delivered}
            onPress={() => props.handleCheck("is_delivered")}
          />
        </View>
        <View style={styles.btn}>
          <PrimaryButton
            title={props.create ? "Create Entry" : "Update Entry"}
            onPress={props.action}
          />
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  constainer: {
    flex: 1,
    flexDirection: "column",
    paddingTop: 12
  },
  miniContainer: {
    flexDirection: "row"
  },
  input: {
    borderColor: "black",
    borderWidth: 1,
    margin: 8,
    borderRadius: 8,
    paddingLeft: 4
  },
  check: {
    flex: 1,
    backgroundColor: "transparent",
    margin: 8
  },
  btn: {
    alignItems: "center"
  }
});

const CustomCheck = props => (
  <CheckBox
    containerStyle={styles.check}
    iconType="antdesign"
    checkedIcon="checkcircle"
    uncheckedIcon="closecircle"
    checkedColor="green"
    uncheckedColor="red"
    {...props}
  />
);

const CustomInput = props => (
  <Input inputContainerStyle={styles.input} {...props} />
);
