import React, { Component } from "react";
import { Mutation } from "react-apollo";
import { Layout, DefaultLoader } from "../Components";
import FormContainer from "./FormContainer";
import { ADD_ENTRY, ALL_ENTRIES } from "../api";

export default class EntryForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      model: "",
      issue: "",
      cust_name: "",
      cust_number: "",
      cust_price: 0,
      repair_cost: 0,
      is_repaired: false,
      is_delivered: false
    };
  }

  handleChange = (field, text) => this.setState({ [field]: text });

  handleCheck = field => this.setState({ [field]: !this.state[field] });

  render() {
    return (
      <Layout title={"New Entry"}>
        <Mutation
          mutation={ADD_ENTRY}
          update={(cache, { data: { createEntry } }) => {
            const data = cache.readQuery({ query: ALL_ENTRIES });
            data.allEntries.push(createEntry);
            cache.writeQuery({
              query: ALL_ENTRIES,
              data
            });
          }}
          variables={{ input: { ...this.state } }}
          onCompleted={() => this.props.navigation.goBack()}>
          {(createEntry, { loading, data, error }) => {
            if (loading) return <DefaultLoader tagline={"Just a moment..."} />;
            return (
              <FormContainer
                handleChange={this.handleChange}
                handleCheck={this.handleCheck}
                data={this.state}
                action={createEntry}
                create={true}
              />
            );
          }}
        </Mutation>
      </Layout>
    );
  }
}
