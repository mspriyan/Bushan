import React, { Component } from "react";
import { Mutation } from "react-apollo";
import { withMappedNavigationParams } from "react-navigation-props-mapper";
import { Layout, DefaultLoader } from "../Components";
import FormContainer from "./FormContainer";
import { UPDATE_ENTRY, ALL_ENTRIES } from "../api";

class UpdateForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...this.props.data
    };
  }

  handleChange = (field, text) => this.setState({ [field]: text });

  handleCheck = field => this.setState({ [field]: !this.state[field] });

  render() {
    const { id, createdAt, __typename, ...input } = this.state;
    return (
      <Layout title={"Edit Entry"}>
        <Mutation
          mutation={UPDATE_ENTRY}
          update={(cache, { data: { updateEntry } }) => {
            const data = cache.readQuery({ query: ALL_ENTRIES });
            const index = data.allEntries.findIndex(entry => entry.id === id);
            data.allEntries[index] = updateEntry;
            cache.writeQuery({
              query: ALL_ENTRIES,
              data
            });
          }}
          variables={{ id: id, input: { ...input } }}
          onCompleted={() => this.props.navigation.goBack()}>
          {(updateEntry, { loading, data, error }) => {
            if (loading) return <DefaultLoader tagline={"Just a moment..."} />;
            return (
              <FormContainer
                handleChange={this.handleChange}
                handleCheck={this.handleCheck}
                data={this.state}
                action={updateEntry}
                create={false}
              />
            );
          }}
        </Mutation>
      </Layout>
    );
  }
}

export default withMappedNavigationParams()(UpdateForm);
