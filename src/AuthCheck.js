import React, { Component } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import { DefaultLoader } from "./Components";

export default class AuthCheck extends Component {
  constructor(props) {
    super(props);
  }

  async componentWillMount() {
    const { navigation } = this.props;
    const token = await AsyncStorage.getItem("TOKEN");
    return navigation.navigate(token ? "App" : "Auth");
  }

  render() {
    return <DefaultLoader tagline={"Starting thrusters..."} />;
  }
}
