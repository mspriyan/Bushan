import React, { Component } from "react";
import { View, Text, TouchableOpacity, Dimensions } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";

export default class Logout extends Component {
  clearToken = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate("Auth");
  };

  render() {
    return (
      <View>
        <Text>Are you sure you want to quit></Text>
        <TouchableOpacity onPress={this.clearToken}>
          <Text>Logout</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
