import React from "react";
import { Dimensions } from "react-native";
import { Button } from "react-native-elements";
import TouchableScale from "react-native-touchable-scale";
import LinearGradient from "react-native-linear-gradient";

const { width: WIDTH } = Dimensions.get("window");

export default function PrimaryButton(props) {
  return (
    <Button
      ViewComponent={LinearGradient}
      TouchableComponent={TouchableScale}
      linearGradientProps={{
        colors: ["#ff4c1d", "#9b0063"],
        start: { x: 0, y: 0 },
        end: { x: 1, y: 0 }
      }}
      containerStyle={styles.btnLogin}
      titleStyle={styles.btnText}
      buttonStyle={{ borderRadius: 25 }}
      {...props}
    />
  );
}

const styles = {
  btnLogin: {
    marginTop: 20,
    borderRadius: 30,
    width: WIDTH - 50,
    height: 55
  },
  btnText: {
    textAlign: "center",
    fontSize: 23,
    color: "rgba(255, 255, 255, 0.8)"
  }
};
