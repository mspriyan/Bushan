import React from "react";
import { Text, View, ActivityIndicator, StyleSheet } from "react-native";

export default function DefaultLoader(props) {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>{props.tagline}</Text>
      <ActivityIndicator color="#ff4c1d" size="large" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    color: "#ff4c1d",
    fontSize: 22,
    textAlign: "center",
    margin: 8
  }
});
