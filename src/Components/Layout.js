import React from "react";
import { StyleSheet } from "react-native";
import { Header, Icon } from "react-native-elements";
import { withNavigation } from "react-navigation";
import TouchableScale from "react-native-touchable-scale";

function Layout(props) {
  return (
    <>
      <Header
        containerStyle={styles.container}
        placement="left"
        backgroundColor="white"
        leftComponent={
          props.menu ? (
            <Menu nav={props.navigation} />
          ) : (
            <Back nav={props.navigation} />
          )
        }
        centerComponent={{
          text: props.title,
          style: { color: "#000", fontWeight: "100", fontSize: 20 }
        }}
        rightComponent={props.rightComponent}
        rightContainerStyle={styles.right}
      />
      {props.children}
    </>
  );
}

const Menu = props => (
  <TouchableScale onPress={() => props.nav.toggleDrawer()} hitSlop={hitSlop}>
    <Icon name="menu" color="#000" size={24} iconStyle={{ padding: 6 }} />
  </TouchableScale>
);

const Back = props => (
  <TouchableScale onPress={() => props.nav.goBack()} hitSlop={hitSlop}>
    <Icon name="arrow-back" color="#000" size={24} />
  </TouchableScale>
);

const hitSlop = { top: 12, left: 12, bottom: 12, right: 12 };

const styles = StyleSheet.create({
  container: {
    height: 56,
    paddingTop: 8
  },
  right: {
    flex: 1,
    flexDirection: "row"
  }
});

export default withNavigation(Layout);
