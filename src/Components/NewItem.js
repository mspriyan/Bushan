import React from "react";
import { StyleSheet } from "react-native";
import { ListItem, Icon } from "react-native-elements";
import { withNavigation } from "react-navigation";
import TouchableScale from "react-native-touchable-scale"; // https://github.com/kohver/react-native-touchable-scale

function NewItem(props) {
  const { model, issue, is_repaired, is_delivered } = props.data;
  return (
    <ListItem
      containerStyle={styles.container}
      Component={TouchableScale}
      friction={5} //
      tension={150} // These props are passed to the parent component (here TouchableScale)
      activeScale={0.9} //// Only if no expo
      leftAvatar={{
        rounded: true,
        overlayContainerStyle: { backgroundColor: colors[props.index % 10] },
        title: model[0].toUpperCase()
      }}
      title={model}
      titleStyle={{ color: "black", fontWeight: "bold" }}
      subtitleStyle={{ color: "black" }}
      subtitle={issue}
      rightIcon={<StatusIcon repair={is_repaired} deliver={is_delivered} />}
      onPress={() => props.navigation.navigate("Detail", { data: props.data })}
    />
  );
}

const StatusIcon = props => {
  const { repair, deliver } = props;
  if (!repair) {
    return <Icon name="closecircle" type="antdesign" color="red" size={24} />;
  } else if (!deliver) {
    return (
      <Icon name="clockcircle" type="antdesign" color="#fbc02d" size={24} />
    );
  } else {
    return <Icon name="checkcircle" type="antdesign" color="green" size={24} />;
  }
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 8,
    borderWidth: 0.5
  }
});

const colors = [
  "#F44336",
  "#373B44",
  "#c0392b",
  "#6e45e2",
  "#d558c8",
  "#2E3192",
  "#1D2B64",
  "#93278F",
  "#FC7D7B",
  "#A3A1FF"
];
export default withNavigation(NewItem);
