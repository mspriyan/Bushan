import {
  createAppContainer,
  createDrawerNavigator,
  createStackNavigator,
  createSwitchNavigator
} from "react-navigation";
import { Login } from "./Login";
import { Home } from "./Home";
import Logout from "./Logout/Logout";
import AuthCheck from "./AuthCheck";
import { CreateForm, UpdateForm } from "./EntryForms";
import Detail from "./EntryDetail/Detail";

const HomeStack = createStackNavigator(
  {
    Homepage: {
      screen: Home
    },
    Create: {
      screen: CreateForm
    },
    Detail: {
      screen: Detail
    },
    Update: {
      screen: UpdateForm
    }
  },
  {
    initialRouteName: "Homepage",
    headerMode: "none"
  }
);

const AppStack = createDrawerNavigator({
  Home: HomeStack,
  Logout: {
    screen: Logout
  }
});

const AuthStack = createStackNavigator({
  Login: {
    screen: Login
  }
});

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthCheck: AuthCheck,
      App: AppStack,
      Auth: AuthStack
    },
    {
      initialRouteName: "AuthCheck"
    }
  )
);
