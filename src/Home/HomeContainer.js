import React, { Component } from "react";
import { View, Text, FlatList, StyleSheet } from "react-native";
import { Button } from "react-native-elements";
import Icon from "react-native-vector-icons/AntDesign";
import { Query } from "react-apollo";
import { DefaultLoader, NewItem, Layout } from "../Components";
import { ALL_ENTRIES } from "../api";

export default class HomeContainer extends Component {
  _keyExtractor = (item, index) => item.id;

  _renderItem = ({ item, index }) => (
    <View style={styles.item}>
      <NewItem data={item} index={index} />
    </View>
  );

  render() {
    return (
      <Layout title="Home" menu={true}>
        <Query query={ALL_ENTRIES}>
          {({ loading, error, data }) => {
            if (loading) {
              return <DefaultLoader tagline={"Intializing sequence..."} />;
            }
            return (
              <View style={styles.container}>
                <FlatList
                  keyExtractor={this._keyExtractor}
                  data={data.allEntries}
                  renderItem={this._renderItem}
                />
                <Button
                  onPress={() => this.props.navigation.navigate("Create")}
                  buttonStyle={styles.fabB}
                  containerStyle={styles.fab}
                  raised
                  icon={<Icon name="plus" size={32} color="#ff4c1d" />}
                />
              </View>
            );
          }}
        </Query>
      </Layout>
    );
  }
}

const styles = new StyleSheet.create({
  container: {
    flex: 1
  },
  item: {
    marginHorizontal: 16,
    marginTop: 5,
    marginBottom: 3
  },
  fab: {
    position: "absolute",
    height: 56,
    width: 56,
    borderRadius: 100,
    bottom: 16,
    right: 18,
    elevation: 6,
    zIndex: 5
  },
  fabB: {
    backgroundColor: "transparent",
    height: 56,
    width: 56,
    borderRadius: 100
  }
});
