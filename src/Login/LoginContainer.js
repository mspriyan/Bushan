import React, { Component } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import { Mutation } from "react-apollo";
import LoginComponent from "./LoginComponent";
import { SIGNIN } from "../api";

export default class LoginContainer extends Component {
  static navigationOptions = {
    header: null
  };

  constructor() {
    super();
    this.state = {
      name: "",
      password: ""
    };
  }

  handleChange = (field, text) => {
    this.setState({ [field]: text });
  };

  _confirm = async data => {
    if (data.signIn) {
      await AsyncStorage.setItem("TOKEN", data.signIn);
      this.props.navigation.navigate("App");
    }
  };
  render() {
    const { name, password } = this.state;
    return (
      <Mutation
        mutation={SIGNIN}
        variables={{ input: { name, password } }}
        onCompleted={data => this._confirm(data)}>
        {(signIn, { loading, error }) => (
          <LoginComponent
            data={this.state}
            action={signIn}
            handleChange={this.handleChange}
            loading={loading}
            error={error}
          />
        )}
      </Mutation>
    );
  }
}
