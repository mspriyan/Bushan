import React from "react";
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  ImageBackground,
  Dimensions
} from "react-native";
import { Button, colors } from "react-native-elements";
import TouchableScale from "react-native-touchable-scale";
import LinearGradient from "react-native-linear-gradient";
import Icon from "react-native-vector-icons/AntDesign";
import bgImage4 from "../../images/bglogin4.jpg";
import { DefaultLoader } from "../Components";

const { width: WIDTH } = Dimensions.get("window");

export default function LoginComponent(props) {
  const { name, password } = props.data;
  let activeScreen;
  let loadingScreen = (
    <View>
      <Icon name="dingding-o" style={styles.logo} />
      <DefaultLoader tagline={"One moment please ..."} />
    </View>
  );

  let loginScreen = (
    <>
      <View>
        <Icon name="dingding-o" style={styles.logo} />
        <Text style={styles.title}>LOGIN</Text>
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          placeholder="Name"
          placeholderTextColor={"rgba(255,255,255,0.5)"}
          onChangeText={text => props.handleChange("name", text)}
          value={name}
        />
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          placeholder="Password"
          secureTextEntry={true}
          placeholderTextColor={"rgba(255,255,255,0.6)"}
          onChangeText={text => props.handleChange("password", text)}
          value={password}
        />
      </View>
      <View>
        <LoginButton onPress={props.action} title="Submit" />
      </View>
    </>
  );

  if (props.loading) {
    activeScreen = loadingScreen;
  } else {
    activeScreen = loginScreen;
  }
  return (
    <ImageBackground source={bgImage4} style={styles.loginContainer}>
      {activeScreen}
    </ImageBackground>
  );
}

const LoginButton = options => (
  <Button
    TouchableComponent={TouchableScale}
    ViewComponent={LinearGradient}
    linearGradientProps={{
      colors: ["#4ea8b0", "#245170"],
      start: { x: 1, y: 0 },
      end: { x: 0.2, y: 0 }
    }}
    containerStyle={styles.btnLogin}
    titleStyle={styles.btnText}
    buttonStyle={{ borderRadius: 25 }}
    {...options}
  />
);

const styles = StyleSheet.create({
  loginContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: null,
    width: null
  },
  logo: {
    fontSize: 80,
    color: "#e0f2f1",
    textAlign: "center"
  },
  title: {
    marginTop: 10,
    fontSize: 30,
    color: "#e0f2f1",
    textAlign: "center"
  },
  inputContainer: {
    marginTop: 20,
    borderColor: "white",
    borderRadius: 25,
    borderWidth: 0.5
  },
  input: {
    borderRadius: 25,
    fontSize: 20,
    height: 50,
    width: WIDTH - 70,
    paddingLeft: 25,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    color: "rgba(255, 255, 255, 0.9)"
  },
  btnLogin: {
    marginTop: 20,
    borderRadius: 30,
    width: WIDTH - 120,
    height: 50
  },
  btnText: {
    textAlign: "center",
    fontSize: 24,
    color: "rgba(255, 255, 255, 0.7)"
  }
});
